<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('fname')->nullable()->after('name');
            $table->string('mname')->nullable()->after('fname');
            $table->string('lname')->nullable()->after('mname');
            $table->char('suffix', 20)->nullable()->after('lname');
            $table->string('username')->nullable()->after('email');
            $table->char('gender', 3)->nullable()->after('suffix');
            $table->string('mobile_number')->nullable()->after('username');
            $table->date('dob')->nullable()->after('gender');
            $table->tinyInteger('type')->default(1)->nullable()->after('remember_token')->comment = '0 - admin | 1 - normal';
            $table->tinyInteger('status')->default(0)->after('type')->comment = '0 - active | 1 - inactive | 2 - blocked | 3 - deleted';
            $table->string('profile_photo')->default('profile.png')->nullable()->after('status');
            $table->string('credential')->nullable()->after('profile_photo');
            $table->tinyInteger('created_by')->nullable()->after('credential');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('fname');
            $table->dropColumn('mname');
            $table->dropColumn('lname');
            $table->dropColumn('suffix');
            $table->dropColumn('username');
            $table->dropColumn('mobile_number');
            $table->dropColumn('type');
            $table->dropColumn('status');
            $table->dropColumn('credential');
            $table->dropColumn('profile_photo');
            $table->dropColumn('created_by');
            $table->dropColumn('gender');
            $table->dropColumn('dob');
        });
    }
}
